<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chapter extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'book_id',
        'number',
        'title',
    ];

    public function book() {
        return $this->belongsTo(Book::class);
    }

    public function questions() {
        return $this->hasMany(Question::class);
    }

    public function tests() {
        return $this->belongsToMany(Chapter::class);
    }
}
