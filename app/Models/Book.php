<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Book extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
    ];

    public function chapters() {
        return $this->hasMany(Chapter::class);
    }
}
