<?php

namespace CHMSSP\Models;

use CHMSSP\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Role extends Model
{
    use Notifiable;

    protected $fillable = [
        'name',
        'default',
    ];

    public function permissions() {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }

    public function users() {
        return $this->belongsToMany(User::class, 'users_roles');
    }

    public function routeNotificationForSlack()
    {
        return env('SLACK_ADMIN_WEBHOOK');
    }
}
