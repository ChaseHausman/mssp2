<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'question_id',
        'letter',
        'text',
        'is_correct',
        'value',
        'explanation'
    ];

    public function question() {
        return $this->belongsTo(Question::class);
    }

    public function responses() {
        return $this->hasMany(Response::class);
    }
}
