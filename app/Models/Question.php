<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'text',
        'chapter_id',
        'hint',
        'points',
        'min_correct_points',
    ];

    public function chapter() {
        return $this->belongsTo(Chapter::class);
    }

    public function answers() {
        return $this->hasMany(Answer::class);
    }

    public function correctAnswers() {
        return $this->answers()->where('is_correct', true)->get();
    }

    public function tests() {
        return $this->belongsToMany(Test::class);
    }
}
