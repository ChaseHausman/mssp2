<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;

class Attempt extends Model
{
    protected $fillable = [
        'test_id',
        'user_id',
        'finished',
        'possible_points',
        'total_points',
        'start_at',
        'stop_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'start_at',
        'stop_at',
    ];

    public function questions() {
        return $this->belongsToMany(Question::class);
    }
}
