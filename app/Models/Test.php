<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'tag',
        'public',
        'specificQuestions',
        'questionCount',
        'user_id',
    ];

    public function creator() {
        return $this->belongsTo(User::class);
    }

    public function chapters() {
        return $this->belongsToMany(Chapter::class);
    }

    public function questions() {
        return $this->belongsToMany(Question::class);
    }
}
