<?php

namespace CHMSSP\Models;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'user_id',
        'attempt_id',
        'question_id',
        'answer_id',
        'selections',
        'final_choice',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function attempt() {
        return $this->belongsTo(Attempt::class);
    }

    public function question() {
        return $this->belongsTo(Question::class);
    }

    public function answer() {
        return $this->belongsTo(Answer::class);
    }
}