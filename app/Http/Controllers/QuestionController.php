<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Answer;
use CHMSSP\Models\Chapter;
use CHMSSP\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    // Limit all book functions to those who can manage books
    public function __construct() {
        $this->middleware('can:manage questions');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Chapter $chapter)
    {
        $questions = $chapter->load('questions', 'questions.answers')->questions;

        return view('admin.questions.index', [
            'chapter' => $chapter,
            'questions' => $questions,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Chapter $chapter)
    {
        return view('admin.questions.create', [
            'chapter' => $chapter,
            'alpha' => [
                'A',
                'B',
                'C',
                'D'
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Chapter $chapter, Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
            'points' => 'required|numeric',
            'min_correct_points' => 'required|numeric',
        ]);

        $question = Question::create([
            'text' => $request->get('text'),
            'hint' => $request->get('hint'),
            'points' => $request->get('points'),
            'chapter_id' => $chapter->id,
            'min_correct_points' => $request->get('min_correct_points'),
        ]);

        foreach($request->get('answer') as $key => $item) {
            if($request->get('answer')[$key] !== null) {
                Answer::create([
                    'question_id' => $question->id,
                    'letter' => $request->get('letter')[$key],
                    'text' => $request->get('answer')[$key],
                    'is_correct' => (isset($request->get('correct')[$key]) ? true : false),
                    'explanation' => $request->get('explanation')[$key],
                    'value' => $request->get('value')[$key],
                ]);
            }
        }

        return redirect(route('questions.create', ['chapter' => $chapter]))->withSuccess("Successfully created a new question.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \CHMSSP\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Chapter $chapter, Question $question)
    {
        return $this->edit($chapter, $question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \CHMSSP\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Chapter $chapter, Question $question)
    {
        return view('admin.questions.edit', [
            'chapter' => $chapter,
            'question' => $question,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \CHMSSP\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Chapter $chapter, Question $question)
    {
        $this->validate($request, [
            'text' => 'required',
            'points' => 'required|numeric',
            'min_correct_points' => 'required|numeric'
        ]);

        $question->update([
            'text' => $request->get('text'),
            'points' => $request->get('points'),
            'min_correct_points' => $request->get('min_correct_points')
        ]);

        return redirect(route('chapters.edit', ['chapter' => $chapter->id, 'question' => $question->id]))
            ->withSuccess("Successfully updated the question.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \CHMSSP\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chapter $chapter, Question $question)
    {
        Answer::where('question_id', $question->id)->delete();

        $question->delete();

        return redirect(route('chapters.index', ['book' => $chapter->book_id]))->withSuccess("Successfully deleted that question and associated answers.");
    }
}
