<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Coasters\Rank;
use CHMSSP\Models\Emails;
use CHMSSP\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class EmailedController extends Controller
{
    public function mailgunUnsubscribe(Request $request) {
        if(!$this->verifyWebhookSignature($request->get('timestamp'), $request->get('token'), $request->get('signature'))) {
            return abort(401);
        }

        try {
            $user = User::where('email', $request->get('recipient'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            Log::info("Received unsubscribe notification for a recipient we wouldn't have sent an email to. [{$request->get('recipient')}]");
            return abort(200);
        }

        $user->setPreference(['email_subscription' => "unsubscribed"]);

        return response('Okay!');
    }

    public function verifyWebhookSignature($timestamp, $token, $signature)
    {
        if (empty($timestamp) || empty($token) || empty($signature)) {
            return false;
        }
        $hmac = hash_hmac('sha256', $timestamp.$token, $this->apiKey);
        if (function_exists('hash_equals')) {
            // hash_equals is constant time, but will not be introduced until PHP 5.6
            return hash_equals($hmac, $signature);
        } else {
            return $hmac === $signature;
        }
    }
}
