<?php

namespace CHMSSP\Http\Controllers;

use Carbon\Carbon;
use CHMSSP\Mail\BallotNotFinished;
use CHMSSP\Models\Coasters\Rank;
use CHMSSP\Models\Emails;
use CHMSSP\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class EmailManagerController extends Controller
{
    public function getCampaignManager() {
        return view('admin.emailer.manager');
    }

    public function getSpecificEmailKind($key) {
        switch ($key) {
            case "unfinished-ballot":
                return view('admin.emailer.unfinished-ballot');
                break;
            default:
                return abort(404);
                break;
        }
    }

    public function postSendSpecificEmail($key, Request $request) {
        switch ($key) {
            case "unfinished-ballot":
                return $this->sendUnfinishedBallot($request);
                break;
            default:
                return abort(404);
                break;
        }
    }

    public function preview($key, Request $request) {
        switch ($key) {
            case "unfinished-ballot":
                return $this->previewUnfinishedBallot($request);
                break;
            default:
                return abort(404);
                break;
        }
    }

    private function sendUnfinishedBallot(Request $request) {
        $ids = Rank::where('ballot_complete', 0)->distinct()->get(['user_id'])->pluck('user_id');
        $users = User::whereIn('id', $ids)->get();

        $message = $request->get('message');
        $deadline = $request->get('deadline');

        if($request->get('deliver_at') !== "") {
            $sendAt = Carbon::parse($request->get('deliver_at'));
        } else {
            $sendAt = Carbon::now();
        }

        $start_at = $sendAt;
        $count = 0;
        foreach($users as $user) {
            $secret = urlencode(base64_encode(random_bytes(16)));
            $email = urlencode(base64_encode($user->email));
            if($user->getPreference('email_subscription') !== "unsubscribe") {
                Mail::to($user)->later($sendAt, new BallotNotFinished($message, $user->getTop5Coasters(), $deadline, $secret, $email));
                Emails::create([
                    'recipient' => $user->email,
                    'type' => 'incomplete_ballot',
                    'secret' => $secret
                ]);
            }

            $count++;
            $sendAt->addSecond();
        }

        $minutes = $count / 60;
        return back()->withSuccess("We've queued {$count} messages to send over {$minutes} minutes start at {$start_at->toDayDateTimeString()}.");
    }

    private function previewUnfinishedBallot(Request $request) {
        $message = $request->get('message');
        $top5 = Auth::user()->getTop5Coasters();
        $deadline = $request->get('deadline');

        return new BallotNotFinished($message, $top5, $deadline, random_bytes(16), Auth::user()->email);
    }
}
