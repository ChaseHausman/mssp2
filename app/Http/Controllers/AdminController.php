<?php

namespace CHMSSP\Http\Controllers;

use Carbon\Carbon;
use CHMSSP\Models\Coasters\Coaster;
use CHMSSP\Models\Coasters\Manufacturer;
use CHMSSP\Models\Coasters\Park;
use CHMSSP\Models\Coasters\Rank;
use CHMSSP\Models\Misc;
use CHMSSP\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function dashboard() {
        $counts = Cache::remember('admin_counts', 60, function() {
            return json_encode([
                'user_objects' => User::withTrashed()->count(),
                'user_active' => User::count(),
                'user_new' => User::where('updated_at', '>=', Carbon::now()->subWeeks(2))->count(),
            ]);
        });

        try {
            $view = view('console', [
                'counts' => json_decode($counts),
            ]);
        } catch (\ErrorException $e) {
            if(str_contains($e->getMessage(), "Undefined")) {
                Cache::forget('admin_counts');
                return redirect(route('console'));
            }

            return abort(500);
        }

        return $view;
    }

    public function search(Request $request) {
        if($request->input('q') == null) {
            return view('admin.search');
        }

        // Search for users
        $users = User::look($request->input('q'));

        // Gather results
        $results = collect($users);

        return view('admin.search', [
            'results' => $results,
            'query' => $request->input('q'),
        ]);
    }
}
