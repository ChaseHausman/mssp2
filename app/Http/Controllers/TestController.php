<?php

namespace CHMSSP\Http\Controllers;

use Carbon\Carbon;
use CHMSSP\Models\Answer;
use CHMSSP\Models\Attempt;
use CHMSSP\Models\Book;
use CHMSSP\Models\Chapter;
use CHMSSP\Models\Response;
use CHMSSP\Models\Test;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function index() {
        $public = Test::where('public', true)->paginate();
        $users = Test::where('public', false)->where('user_id', Auth::id())->get();

        return view('tests.index', [
            'public' => $public,
            'users' => $users,
        ]);
    }

    public function createNewTest() {
        if(!Auth::check()) {
            return abort(404);
        }

        if(!(Auth::user()->can('make tests') || Auth::user()->can('manage tests'))) {
            return abort(404);
        }

        return view('tests.create', [
            'books' => Book::with('chapters')->get(),
        ]);
    }

    public function create(Request $request) {
        $this->validate($request, [
            'tag' => 'required',
            'questionCount' => 'required|numeric',
            'specificQuestions' => 'required',
        ]);

        $test = Test::create([
            'tag' => $request->get('tag'),
            'public' => false,
            'questionCount' => $request->get('questionCount'),
            'user_id' => Auth::id(),
        ]);

        $failed = false;
        foreach($request->get('chapters') as $id) {
            try {
                $chapter = Chapter::where('id', $id)->firstOrFail();
            } catch (ModelNotFoundException $e) {
                $failed = true;
            }

            $test->chapters()->attach($chapter->id);
        }

        if(Auth::user()->can('manage tests') && $request->get('public')) {
            $test->update([
                'public' => $request->get('public'),
            ]);
        }

        $response = redirect(route('tests.landing', ['test' => $test->id]))->withSuccess("Successfully created that test!");

        if($failed) {
            $response->withWarning("At least one chapter could not be found, and was therefore omitted from the test.");
        }

        return $response;
    }

    public function landing(Test $test) {
        if(!Auth::check() || !Auth::user()->can('take tests')) {
            return abort(404);
        }

        $attempts = Attempt::where('user_id', Auth::id())->where('test_id', $test->id)->get();

        return view('tests.landing', [
            'test' => $test,
            'attempts' => $attempts,
        ]);
    }

    public function attempt(Test $test, Request $request) {
        // First, we need to use the test to determine what questions we're going to use
        if($test->specificQuestions) {
            $questions = $test->questions;
        } else {
            $chapters = $test->chapters->load('questions');

            $questions = new Collection();
            foreach($chapters as $chapter) {
                $questions->push($chapter->questions);
            }

            $questions = $questions->flatten();

            try {
                $questions = $questions->random($test->questionCount);
            } catch (\InvalidArgumentException $e) {
                return back()->withDanger("There weren't enough questions in this chapter to fill the required number of questions.");
            }
        }

        $attempt = Attempt::create([
            'user_id' => Auth::id(),
            'test_id' => $test->id,
            'finished' => false,
            'start_at' => Carbon::now(),
            'end_at' => null,
        ]);

        $attempt->questions()->sync($questions->pluck('id'));

        return redirect(route('tests.take', ['test' => $test->id, 'attempt' => $attempt->id]))->withSuccess("Good Luck!");
    }

    public function take(Test $test, Attempt $attempt) {
        if(!Auth::check() || !Auth::user()->can('take tests')) {
            return abort(404);
        }

        if(!$test->public && $test->user_id !== Auth::id()) {
            return abort(403);
        }

        $attempt->load('questions', 'questions.answers');

        return view('tests.take', [
            'test' => $test,
            'attempt' => $attempt,
        ]);
    }

    public function recordResponse(Test $test, Request $request) {
        return response()->json($request->all());
    }

    public function submit(Test $test, Attempt $attempt, Request $request) {
        if(!Auth::check() || !Auth::user()->can('take tests')) {
            return abort(404);
        }

        $answers = [];
        foreach($request->get('submission') as $answer => $checked) {
            $answers[] = $answer;

            $ans = Answer::where('id', $answer)->first();

            Response::updateOrCreate([
                'user_id' => Auth::id(),
                'attempt_id' => $attempt->id,
                'question_id' => $ans->question_id,
                'answer_id' => $ans->id,
            ],[
                'selections' => 1,
                'final_choice' => true,
            ]);
        }

        $questions = $attempt->questions;

        $points = 0;
        $total = 0;
        foreach($questions as $question) {
            $total = $total + $question->points;

            $responses = Response::where('user_id', Auth::id())->where('attempt_id', $attempt->id)->where('question_id', $question->id)->get();
            foreach($responses as $resp) {

            }
        }
    }
}
