<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Coasters\Category;
use CHMSSP\Models\Coasters\Coaster;
use CHMSSP\Models\Coasters\Type;
use CHMSSP\Models\Contact;
use CHMSSP\Models\Role;
use CHMSSP\Models\User;
use CHMSSP\Notifications\ContactUs;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ContactController extends Controller
{
    public function general(Request $request) {
        return view('contact.general', [
            'subject' => $request->input('about'),
        ]);
    }

    public function post(Request $request) {
        if(!Auth::check()) {
            $client = new Client();
            $recaptcha = $client->post("https://www.google.com/recaptcha/api/siteverify", [
                'form_params' => [
                    'secret' => env('RECAPTCHA_SECRET_KEY'),
                    'response' => $request->input('g-recaptcha-response'),
                ]
            ]);

            if(!json_decode($recaptcha->getBody())->success) {
                return back()->withWarning("Hmm. Google think's you're a bot. We'll be unable to continue.");
            }
        }

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        $user = User::where('email', $request->input('email'))->first();

        $contact = Contact::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'message' => $request->input('message'),
            'extra' => (isset($extra)) ? $extra : null,
        ]);

        $contact->user()->associate($user);

        if(isset($model)) {
            $model->contact()->save($contact);
        }

        $recipients = Role::where('name', 'Admin')->first()->users;
        foreach($recipients as $recipient) {
            $recipient->notify(new ContactUs($contact));
        }

        return redirect(route('home'))->withSuccess("We'll be checking on that shortly.");
    }

    public function admin($id = null) {
        if ($id === null) {
            return view('contact.contacts', [
                'contacts' => Contact::with('user')->orderBy('created_at', 'DESC')->paginate(25),
            ]);
        }

        try {
            $contact = Contact::where('id', $id)->with('user', 'contactable')->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return abort(404);
        }

        return view('contact.admin', [
            'contact' => $contact
        ]);
    }
}
