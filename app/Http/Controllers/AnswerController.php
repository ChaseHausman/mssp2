<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Answer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'answer' => 'required',
            'letter' => 'required|alpha',
            'value' => 'required|numeric',
        ]);

        Answer::create([
            'question_id' => $request->get('question'),
            'text' => $request->get('answer'),
            'letter' => $request->get('letter'),
            'value' => $request->get('value'),
            'explanation' => $request->get('explanation'),
            'is_correct' => ($request->get('correct') !== null) ? true : false,
        ]);

        return back()->withSuccess("Successfully created that answer option.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \CHMSSP\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \CHMSSP\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \CHMSSP\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        $this->validate($request, [
            'answer' => 'required',
            'letter' => 'required|alpha',
            'value' => 'required|numeric',
        ]);

        $answer->update([
            'text' => $request->get('answer'),
            'letter' => $request->get('letter'),
            'value' => $request->get('value'),
            'explanation' => $request->get('explanation'),
            'is_correct' => ($request->get('correct') !== null) ? true : false,
        ]);

        return back()->withSuccess("Successfully updated that answer option.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \CHMSSP\Models\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        //
    }
}
