<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Answer;
use CHMSSP\Models\Book;
use CHMSSP\Models\Chapter;
use CHMSSP\Models\Question;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function index() {
        $books = Book::with('chapters')->get();

        return view('quiz.index', [
            'books' => $books,
        ]);
    }

    public function take(Request $request, $chapter = null) {
        // Determine what questions are allowed to be chosen.
        if($chapter == null) {
            if($request->get('chapter') != null && $request->get('chapter') != 0){
                $questions = Question::where('chapter_id', $request->get('chapter'))->get()->pluck('id');
            } else {
                $questions = Question::select('id')->get()->pluck('id');
            }
        } else {
            $questions = Question::where('chapter_id', $chapter)->get()->pluck('id');
        }

        // Loop through trying random numnbers until we find one that is a valid question ID.
        $valid = false;
        $tries = 0;
        while(!$valid) {
            // Generate a random number that is within the bounds of the ids we're using.
            $random = rand($questions->first(), $questions->last());

            $question = Question::where('id', $random)->with('answers')->first();

            if($question !== null) {
                $valid = true;
            }

            $tries++;

            if($tries > $questions->count()) {
                return back()->withDanger("Took over {$questions->count()} tries and we couldn't find a question that would work.");
            }
        }

        if(!isset($question)) {
            return back()->withDanger("Unabled to find valid question.");
        }

        $question->load('chapter.book', 'answers');

        return view('quiz.take', [
            'question' => $question,
        ]);
    }

    public function attempt(Request $request) {
        try {
            $answer = Answer::where('id', $request->get('answer'))->where('question_id', $request->get('question'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return abort(404);
        }

        if($answer->is_correct) {
            $type = "success";
            $title = "Correct!";
            $message = "Go to a new question?";
            $goback = true;
            $confirm = "New Question";
            $cancel = "Stay Here";
        } else {
            $type = "error";
            $title = "Incorrect";
            $message = "Sorry, that's not right. Try again?";
            $goback = false;
            $confirm = "New Question";
            $cancel = "Try Again";
        }

        return response()->json([
            'type' => $type,
            'title' => $title,
            'message' => $message,
            'goback' => $goback,
            'confirm' => $confirm,
            'cancel' => $cancel,
        ]);
    }
}
