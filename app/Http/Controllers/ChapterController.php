<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Book;
use CHMSSP\Models\Chapter;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ChapterController extends Controller
{
    // Limit all book functions to those who can manage books
    public function __construct() {
        $this->middleware('can:manage books');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Book $book)
    {
        $chapters = Chapter::where('book_id', $book->id)->orderBy('number', 'ASC')->get();

        return view('admin.chapters.chapters', [
            'book' => $book,
            'chapters' => $chapters,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'number' => 'numeric|required',
            'title' => 'required',
            'book' => 'required'
        ]);

        try {
            $book = Book::where('id', $request->get('book'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return back()->withDanger("Unable to find the book ({$request->book}) this belongs to.");
        }

        $chapter = Chapter::create([
            'book_id' => $book->id,
            'number' => $request->get('number'),
            'title' => $request->get('title'),
        ]);

        return back()->withSuccess("Successfully created that chapter!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \CHMSSP\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book, Chapter $chapter)
    {
        return view('admin.chapters.show', [
            'questions' => $chapter->questions,
            'book' => $book,
            'chapter' => $chapter,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \CHMSSP\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book, Chapter $chapter)
    {
        return view('admin.chapters.manage', [
            'book' => $book,
            'chapter' => $chapter,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \CHMSSP\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book, Chapter $chapter)
    {
        $this->validate($request, [
            'number' => 'numeric|required',
            'title' => 'required',
        ]);

        $chapter = $chapter->update([
            'number' => $request->get('number'),
            'title' => $request->get('title'),
        ]);

        return redirect(route('chapters.index', ['book' => $book->id]))->withSuccess("Updated Chapter {$chapter->number}.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \CHMSSP\Models\Chapter  $chapter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book, Chapter $chapter)
    {
        $number = $chapter->number;

        $chapter->delete();

        return redirect(route('chapters.index', ['book' => $book->id]))->withSuccess("Deleted Chapter {$number}.");
    }
}
