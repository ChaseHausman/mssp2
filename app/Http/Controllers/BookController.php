<?php

namespace CHMSSP\Http\Controllers;

use CHMSSP\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    // Limit all book functions to those who can manage books
    public function __construct() {
        $this->middleware('can:manage books');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.books.books', [
            'books' => Book::with('chapters')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $book = Book::create([
            'title' => $request->get('title')
        ]);

        return redirect(route('chapters.index', ['book' => $book->id]))->withSuccess("We've created a new book! Now we need to add a few chapters.");
    }

    /**
     * Display the specified resource.
     *
     * @param  \CHMSSP\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return redirect(route('chapters.index', ['book' => $book->id]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \CHMSSP\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('admin.books.manage', [
            'book' => $book,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \CHMSSP\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $book->update([
            'title' => $request->get('title'),
        ]);

        return redirect(route('books.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \CHMSSP\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return redirect(route('books.index'))->withSuccess("It's Gone!");
    }
}
