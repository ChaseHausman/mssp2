<?php

namespace CHMSSP\Console\Commands;

use CHMSSP\Models\Question;
use Illuminate\Console\Command;

class CheckForAnswers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'questions:answers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks that every question has at least one correct answer.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $questions = Question::whereHas('answers')->with('answers')->get();

        $ids = [];

        foreach($questions as $question) {
            $hasCorrectAnswer = false;
            foreach($question->answers as $answer) {
                if($answer->is_correct) {
                    $hasCorrectAnswer = true;
                }
            }

            if($hasCorrectAnswer == false) {
                $ids[] = $question->id;
            }
        }

        if(sizeof($ids) == 0) {
            return $this->info("All questions have at least one answer.");
        }

        $this->info("The following question IDs do not have a correct answer.");
        foreach($ids as $id) {
            $this->info($id);
        }
    }
}
