<?php

namespace CHMSSP\Helpers;

use CHMSSP\Models\User;

class CPID {
    public $prefix;
    private $id;
    public $thing = null;

    public function __construct($cid) {
        if($cid != null) {
            $this->prefix = substr($cid, 0, 1);
            $this->id = substr($cid, 1);

            $this->thing = $this->findThing($this->prefix, $this->id);
        }
    }

    public function __toString() {
        return $this->prefix.$this->id;
    }

    public function findThing($prefix, $id) {
        switch ($prefix) {
            case "U":
                return User::where('id', $id)->first();
                break;
            default:
                return null;
        }
    }

    public function getClassName() {
        if($this->thing !== null) {
            return get_class($this->thing);
        }

        return "";
    }

    public function getNameOrTitle() {
        // Things with titles
        if(in_array($this->prefix, ['L'])) {
            return $this->thing->title;
        }

        if(in_array($this->prefix, ['C', 'P', 'M'])) {
            return $this->thing->name;
        }

        if($this->prefix == "U") {
            return $this->thing->handle;
        }

        return "Unknown";
    }
}