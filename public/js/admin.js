/**
 * Created by Chase on 5/22/18.
 */

$('.confirm-form').on('click', function(e) {
    console.log("Confirmation...");
    e.preventDefault();
    var btn = $(this);
    bootbox.confirm({
        message: "Are you sure?",
        buttons: {
            confirm: {
                label: "Do it.",
                className: "btn-primary"
            },
            cancel: {
                label: "Nevermind!",
                className: "btn-secondary"
            }
        },
        callback: function(result) {
            if(result) {
                btn.off('click').click();
            }
        }
    })
});