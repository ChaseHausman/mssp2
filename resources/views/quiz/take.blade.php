@extends('layouts.app')

@section('title')
    Quiz
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('quiz') }}">Quizzes</a></li>
        <li class="breadcrumb-item active">Question</li>
    </ol>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <h2 class="card-title">Do You Know?</h2>
                </div>
                <div class="card-block">
                    <p class="card-text">{{ $question->text }}</p>
                    @foreach($question->answers as $answer)
                        <div class="form-group">
                            <button class="btn btn-outline-primary btn-block answer" style="white-space: normal" type="button" data-answer="{{ $answer->id }}" data-question="{{ $answer->question_id }}">{{ $answer->text }}</button>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/sweetalert2"></script>
    <script src="https://unpkg.com/promise-polyfill"></script>
    <script>
        $('.answer').on("click", function(e) {
            let button = $(e.target);
            let answer = button.data('answer');
            let question = button.data('question');

            $.post({
                url: "{{ route('quiz.attempt') }}",
                method: "POST",
                data: {
                    question: question,
                    answer: answer,
                },
                success: function(resp) {
                    if(resp.type === 'error') {
                        button.addClass('btn-outline-danger').removeClass('btn-outline-primary').prop('disabled', 'disabled');
                    } else if(resp.type === 'success') {
                        button.addClass('btn-outline-success').removeClass('btn-outline-primary').prop('disabled', 'disabled');
                    }

                    Swal({
                        title: resp.title,
                        text: resp.message,
                        type: resp.type,
                        showCancelButton: true,
                        confirmButtonText: resp.confirm,
                        cancelButtonText: resp.cancel,
                    }).then((result => {
                        if(result.value) {
                            location.reload(true);
                        }
                    }));
                },
                error: function() {
                    Swal({
                        title: "That didn't work.",
                        text: "Something happened and we're not sure what.",
                        type: "warning",
                    });
                }
            })
        });
    </script>
@endsection