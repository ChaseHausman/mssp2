@extends('layouts.app')

@section('title')
    Quiz
@endsection

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">Quizzes</li>
    </ol>
    <div class="row">
        <div class="col-md-6 offset-3">
            <div class="card card-outline-primary">
                <div class="card-header">
                    <h2 class="card-title">New Quiz</h2>
                </div>
                <div class="card-block">
                    <form action="{{ route('quiz.take') }}">
                        <p>You can either select a chapter to get a question from, or get one from anywhere in the book.</p>
                        <div class="form-group">
                            <label for="chapter">Chapter</label>
                            <select name="chapter" class="form-control" id="chapter">
                                <option value="0">Anything</option>
                                @foreach($books as $book)
                                    <optgroup label="{{ $book->title }}">
                                        @foreach($book->chapters as $chapter)
                                            <option value="{{ $chapter->id }}">{{ $chapter->title }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-outline-success btn-block">Start Quiz</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection