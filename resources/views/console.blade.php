@extends('layouts.admin')

@section('title')
Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.user.index') }}"><i class="fa fa-users"></i> Users</a>
                </div>
                <div class="card-block">
                    <table class="table">
                        <tr>
                            <th>Total Users</th>
                            <td>{{ $counts->user_objects }}</td>
                        </tr>
                        <tr>
                            <th>Active Users</th>
                            <td>{{ $counts->user_active }}</td>
                        </tr>
                        <tr>
                            <th>New* Users</th>
                            <td>{{ $counts->user_new }}</td>
                        </tr>
                    </table>
                    <small>*Created in the last two weeks.</small>
                </div>
            </div>
        </div>
    </div>
@endsection
