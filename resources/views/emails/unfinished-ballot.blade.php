@component('mail::message')
# The Poll Is Closing!

{{ $message }}

Below are your top {{ $top5->count() }} coasters, as far as we know.

@component('mail::table')
| Coaster | Park | Rank |
|---------|:----:|------|
@foreach($top5 as $coaster)
|{{ $coaster->coaster->name }}|{{ $coaster->coaster->park->name }}|{{ $coaster->rank }}|
@endforeach
@endcomponent

If you think everything's right, just click the link below and you're done! Or, if something is wrong, you can make changes until {{ $deadline }}.

@component('mail::button', ['url' => url(route('emailer.ballots.complete', $links)), 'color' => 'green'])
These Are Correct
@endcomponent
@component('mail::button', ['url' => url(route('emailer.ballots.incomplete', $links)), 'color' => 'red'])
Something's Wrong
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
