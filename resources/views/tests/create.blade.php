@extends('layouts.app')

@section('title')
    New Test
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card card-block">
                <h2 class="card-title">Create New Test</h2>
                <form action="{{ route('tests.create.post') }}" method="post" class="row">
                    <div class="form-group col-sm-12">
                        <label for="tag">Test Tag</label>
                        <input type="text" id="tag" name="tag" class="form-control">
                    </div>
                    @can('manage tests')
                        <div class="form-group col-sm-12">
                           <label for="public">Make Public</label>
                            <select name="public" id="public" class="form-control">
                                <option value="0">Private</option>
                                <option value="1">Public</option>
                            </select>
                        </div>
                    @endcan
                    <div class="form-group col-sm-12">
                        <label for="chapters">Chapters</label>
                        <select name="chapters[]" multiple size="16" id="chapters" class="form-control">
                            @foreach($books as $book)
                                <optgroup label="{{ $book->title }}">
                                    @foreach($book->chapters as $chapter)
                                        <option value="{{ $chapter->id }}">{{ $chapter->number }}: {{ $chapter->title }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="questionCount">Number of Questions</label>
                        <input type="number" id="questionCount" name="questionCount" class="form-control">
                    </div>
                    <div class="col-sm-6">
                        <label for="specificQuestions">Questions</label>
                        <select type="select" id="specificQuestions" name="specificQuestions" class="form-control">
                            <option value="0">Randomize From Chapters</option>
                            <option value="1">Use Specified Questions</option>
                        </select>
                        <small class="help-block">If question count is larger than the specified questions, we will automatically make up the difference in random questions.</small>
                    </div>
                    <div class="form-group col-sm-12">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection