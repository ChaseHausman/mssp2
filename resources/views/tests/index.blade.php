@extends('layouts.app')

@section('title')
    Available Tests
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">Public Tests</h2>
                </div>
                <div class="card-block">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Test</th>
                                <th>Questions</th>
                                <th>Type</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($public as $test)
                                <tr>
                                    <td><a href="{{ route('tests.landing', ['test' => $test->id]) }}">{{ $test->tag }}</a></td>
                                    <td>{{ $test->questsionCount }}</td>
                                    <td>
                                        @if($test->specificQuestions)
                                            <i class="fa fa-sort-alpha-asc fa-fw" title="Specific Questions"></i>
                                        @else
                                            <i class="fa fa-random fa-fw" title="Randomized Questions"></i>
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
    </div>
@endsection

@section('scripts')

@endsection