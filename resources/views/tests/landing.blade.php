@extends('layouts.app')

@section('title')
    {{ $test->tag }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8 offset-md-2">
            <div class="card">
                <div class="card-header">
                    <h2 class="card-title">{{ $test->tag }} @if(Auth::user()->can('manage tests') || ($test->id == Auth::id() && Auth::user()->can('make tests')))<span class="pull-right"><a href="{{ route('tests.manage', ['test' => $test->id]) }}" class="btn btn-outline-primary"><i class="fa fa-cogs"></i></a></span> @endif </h2>
                    <p><span class="badge badge-info">{{ $test->questionCount }} Questions</span> @if($test->specificQuestions)<span class="badge badge-success">Specific Questions</span>@else <span class="badge badge-primary">Random Questions</span>@endif </p>
                </div>
                <div class="card-block">
                    <p>Test covers the following chapters:</p>
                    <ul class="mt-3">
                        @foreach($test->chapters as $chapter)
                            <li>{{ sprintf("%0d", $chapter->number) }}: {{ $chapter->title }}</li>
                        @endforeach
                    </ul>
                    <a href="{{ route('tests.attempt', ['test' => $test->id]) }}" class="btn btn-primary">New Attempt</a>
                    <table class="table table-striped mt-4">
                        @foreach($attempts as $attempt)
                            <tr>
                                <td><a href="{{ route('tests.take', ['test' => $test->id, 'attempt' => $attempt->id]) }}">Attempt started at {{ date_format($attempt->created_at, "g:ia m/d/Y")  }}</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection