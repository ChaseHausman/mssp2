@extends('layouts.app')

@section('title')
    Take {{ $test->tag }}
@endsection

@section('content')
    <form action="{{ route('tests.submit.post', ['test' => $test->id, 'attempt' => $attempt->id]) }}" method="post">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                @foreach($attempt->questions as $question)
                    <div class="card mb-4">
                        <div class="card-header">
                            {{ $loop->iteration }}
                        </div>
                        <div class="card-block">
                            <p class="card-text">{{ $question->text }}</p>
                            @foreach($question->answers as $answer)
                                <div class="btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-outline-primary btn-block" style="white-space: normal">
                                        <input type="checkbox" name="submissions[{{ $answer->id }}]" autocomplete="off"> {{ $answer->text }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="card card-outline-primary card-block">
                    {{ csrf_field() }}
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <button type="submit" name="final" value="1" class="btn btn-primary btn-block">Submit and Grade <i class="fa fa-check"></i></button>
                        </div>
                        <div class="col-sm-6">
                            <button type="submit" name="final" value="0" class="btn btn-secondary btn-block">Save For Later <i class="fa fa-save"></i> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script>

    </script>
@endsection