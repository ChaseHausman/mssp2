<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto my-auto">
        @if(\Illuminate\Support\Facades\Auth::check() && Auth::user()->can('take quiz'))
            <li class="nav-item"><a href="{{ route('quiz') }}" class="nav-link">Quiz</a></li>
        @endif
        @if(Auth::check() && (Auth::user()->can('take tests') || Auth::user()->can('manage tests')))
            <li class="nav-item"><a href="{{ route('tests') }}" class="nav-link">Tests</a></li>
        @endif
        @if(Auth::check() && (Auth::user()->can('make tests') || Auth::user()->can('manage tests')))
            <li class="nav-item"><a href="{{ route('tests.create') }}" class="nav-link">Create</a></li>
        @endif
    </ul>
    <ul class="navbar-nav">
        @if (Auth::guest())
            <li class="nav-item p-1">
                <a class="btn btn-outline-success" href="{{ route('register') }}"><i class="fa fa-hand-peace-o"></i> Sign Up</a>
            </li>
            <li class="nav-item p-1">
                <a class="btn btn-outline-info" href="{{ route('login') }}"><i class="fa fa-sign-in"></i> Sign In</a>
            </li>
        @else
            @role('Admin')
            <li class="nav-item"><a href="{{ route('admin') }}" class="nav-link">Admin Console</a></li>
            @endrole
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" id="notifications-dropdown" data-count="{{ $_notifications->count() }}">
                    @if($_notifications->count() > 0)<span class="badge badge-success" id="notification-badge">{{ $_notifications->count() }}</span>@endif
                    <i class="fa @if($_notifications->count() > 0) fa-bell text-warning @else fa-bell-o @endif" id="notifications-icon"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach($_notifications as $notification)
                        <a class="dropdown-item notification py-0 my-1" href="{{ $notification->data['link'] }}" data-notification="{{ $notification->id }}">
                            <div class="container-fluid mx-0 px-1 notification-container">
                                <div class="card card-block p-1">
                                    <h6 class="lead">@if($notification->read_at == null)<i class="fa fa-circle-o text-info unread-dot"></i> @endif{{ $notification->data['title'] }}</h6>
                                    <p class="my-1">{{ $notification->data['body'] }}</p>
                                </div>
                            </div>
                        </a>
                    @endforeach
                    <a class="dropdown-item py-0 my-1" href="{{ route('notifications') }}">
                        <div class="container-fluid mx-0 px-1 notification-container">
                            <div class="card card-block p-1 text-center">
                                <p class="lead mb-0">All Notifications</p>
                            </div>
                        </div>
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{ Auth::user()->name }}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('profile', ['handle' => \Illuminate\Support\Facades\Auth::user()->handle]) }}">Your Profile</a>
                    <a class="dropdown-item" href="{{ route('user.settings') }}">Account Settings</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                    ><i class="fa fa-fw fa-sign-out"></i> Sign Out</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        @endif
    </ul>
</div>