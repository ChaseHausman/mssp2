@extends('layouts.admin')

@section('title')
    {{ $chapter->title }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-2">
            @can('manage questions')
                <a href="{{ route('questions.index', ['chapter' => $chapter->id]) }}" class="btn btn-outline-secondary btn-block">Manage Questions</a>
            @endcan
        </div>
        <div class="col-md-8">
            <table class="table table-striped">
                <tbody>
                    @foreach($questions as $question)
                        <tr>
                            <td>{{ str_limit($question->text, 100) }}</td>
                            <td>{{ $question->answers->count() }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')

@endsection