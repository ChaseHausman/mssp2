@extends('layouts.admin')

@section('title')
    {{ $book->title }} Chapters
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Num</th>
                    <th>Title</th>
                    <th>Questions</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($chapters as $chapter)
                        <tr>
                            <th>{{ $chapter->number }}</th>
                            <td><a href="{{ route('chapters.show', ['book' => $chapter->book_id, 'chapter' => $chapter->id]) }}">{{ $chapter->title }}</a></td>
                            <td>{{ ($chapter->questions !== null) ? $chapter->questions->count() : 0 }}</td>
                            <td>
                                <form action="{{ route('chapters.destroy', ['book' => $chapter->book_id, 'chapter' => $chapter->id]) }}" method="post">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <div class="btn-group btn-group-sm">
                                        @can('manage questions')
                                            <a href="{{ route('questions.index', ['chapter' => $chapter->id]) }}" class="btn btn-outline-secondary">Questions</a>
                                        @endcan
                                        <a href="{{ route('chapters.edit', ['book' => $chapter->book_id, 'chapter' => $chapter->id]) }}" class="btn btn-sm btn-outline-primary">Manage</a>
                                        <button type="submit" class="btn btn-sm btn-danger confirm-form">Delete</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <div class="card card-block">
                <h2>Add New Chapter</h2>
                <form action="{{ route('chapters.store', ['book' => $book->id]) }}" method="post">
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label for="number">Num.</label>
                            <input type="number" name="number" class="form-control" id="number" value="{{ ($chapters->last()->number + 1) ?? 1 }}">
                        </div>
                        <div class="col-sm-8">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" id="title" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ csrf_field() }}
                        <input type="hidden" name="book" value="{{ $book->id }}">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection