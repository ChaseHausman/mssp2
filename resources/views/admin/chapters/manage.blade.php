@extends('layouts.admin')

@section('title')
    Update #{{ $chapter->number }}
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <form class="card card-block" action="{{ route('chapters.update', ['book' => $book->id, 'chapters' => $chapter->id]) }}" method="post">
                <div class="form-group">
                    <label for="number">Chapter Number</label>
                    <input type="number" class="form-control" id="number" name="number" value="{{ $chapter->number }}">
                </div>
                <div class="form-group">
                    <label for="title">Chapter Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ $chapter->title }}">
                </div>
                <div class="form-group">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

@endsection