@extends('layouts.admin')

@section('title')
    {{ $chapter->title }}'s Questions
@endsection

@section('content')
    <a href="{{ route('chapters.index', ['book' => $chapter->book_id]) }}"><i class="fa fa-backward"></i> Return to {{ $chapter->book->title }}</a><br>
    <div class="row mt-4">
        <div class="col-md-9">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Question</th>
                        <th>Options</th>
                        <th></th>
                    </tr>
                </thead>
                @foreach($questions as $question)
                    <tr>
                        <td><a href="{{ route('questions.show', ['chapter' => $chapter->id, 'question' => $question->id]) }}">{{ $question->id }}</a></td>
                        <td>{{ str_limit($question->text, 70) }}</td>
                        <td>{{ $question->answers->count() }}</td>
                        <td>
                            <form action="{{ route('questions.destroy', ['chapter' => $chapter->id, 'question' => $question->id]) }}" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-sm">
                                    <a href="{{ route('questions.show', ['chapter' => $chapter->id, 'question' => $question->id]) }}" class="btn btn-outline-primary btn-sm">Manage</a>
                                    <button type="submit" class="btn btn-sm btn-danger confirm-form">Delete</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
        <div class="col-md-3">
            <div class="card card-block">
                <ul class="list-unstyled">
                    <li><a href="{{ route('books.show', ['book' => $chapter->book_id]) }}">{{ $chapter->book->title }}</a></li>
                    <li>Chapter #{{ $chapter->number }}</li>
                </ul>
                <a href="{{ route('questions.create', ['chapter' => $chapter->id]) }}">Create Question</a>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection