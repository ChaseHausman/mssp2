@extends('layouts.admin')

@section('title')
    Question #{{ $question->id }}
@endsection

@section('content')
    <a href="{{ route('questions.index', ['chapter' => $chapter->id]) }}" class="mb-4"><i class="fa fa-arrow-circle-left"></i> Back to {{ $chapter->title }}</a>
    <div class="row mt-4">
        <div class="col-md-4">
            <form action="{{ route('questions.update', ['chapter' => $chapter->id, 'question' => $question->id]) }}" method="post" class="card card-block">
                <div class="form-group">
                    <label for="text">Question Text</label>
                    <textarea name="text" id="text" class="form-control" rows="6">{{ old('text') ?? $question->text }}</textarea>
                </div>
                <div class="form-group">
                    <label for="hint">Hint</label>
                    <textarea name="hint" id="hint" class="form-control">{{ old('hint') ?? $question->hint }}</textarea>
                </div>
                <div class="form-group">
                    <label for="points">Points</label>
                    <input type="number" name="points" id="points" class="form-control" value="1">
                </div>
                <div class="form-group">
                    <label for="min-correct">Minimum Correct Points</label>
                    <input type="number" name="min_correct_points" id="min-correct" class="form-control" value="1">
                </div>
                <hr>
                <div class="form-group">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <input type="hidden" name="chapter" value="{{ $chapter->id }}">
                    <button type="submit" class="btn btn-primary btn-block">Update</button>
                </div>
            </form>
        </div>
        <div class="col-md-8">
            <div class="card card-block">
                <h3>Answer Choices</h3>
                @foreach($question->answers as $answer)
                    <form action="{{ route('answers.update', ['answer' => $answer]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group row option">
                            <div class="col-sm-2">
                                <label for="letter-{{ $answer->id }}">Letter</label>
                                <input type="text" class="form-control" name="letter" id="letter-{{ $answer->id }}" value="{{ $answer->letter }}">
                            </div>
                            <div class="col-sm-8">
                                <label for="answer-{{ $answer->id }}">Option</label>
                                <input type="text" class="form-control" name="answer" id="answer-{{ $answer->id }}" value="{{ $answer->text }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Correct?</label>
                                <div class="btn-group-toggle" data-toggle="buttons">
                                    <label for="answer-{{ $answer->id }}-correct" class="btn btn-outline-secondary btn-block @if($answer->is_correct)active @endif">
                                        <input type="checkbox" class="form-check-input" id="answer-{{ $answer->id }}-correct" name="correct" @if($answer->is_correct) checked="checked" @endif> Yes
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <label for="explanation-{{ $answer->id }}">Explanation</label>
                                <input type="text" class="form-control" name="explanation" id="explanation-{{ $answer->id }}" value="{{ $answer->explanation }}">
                            </div>
                            <div class="col-sm-2">
                                <label for="value-{{ $answer->id }}">Value</label>
                                <input type="number" class="form-control" name="value" id="value-{{ $answer->id }}" value="{{ $answer->value }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Submit</label>
                                <button type="submit" class="btn btn-primary btn-block">Update</button>
                            </div>
                        </div>
                    </form>
                @endforeach
                <form action="{{ route('answers.store') }}" method="post" class="bg-faded">
                    {{ csrf_field() }}
                    <input type="hidden" name="question" value="{{ $question->id }}">
                    <div class="form-group row option">
                        <div class="col-sm-2">
                            <label for="letter-0">Letter</label>
                            <input type="text" class="form-control" name="letter" id="letter-0">
                        </div>
                        <div class="col-sm-8">
                            <label for="answer-0">Option</label>
                            <input type="text" class="form-control" name="answer" id="answer-0">
                        </div>
                        <div class="col-sm-2">
                            <label>Correct?</label>
                            <div class="btn-group-toggle" data-toggle="buttons">
                                <label for="answer-0-correct" class="btn btn-outline-secondary btn-block">
                                    <input type="checkbox" class="form-check-input" id="answer-0-correct" name="correct" @if($answer->is_correct) checked="checked" @endif> Yes
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <label for="explanation-0">Explanation</label>
                            <input type="text" class="form-control" name="explanation" id="explanation-0">
                        </div>
                        <div class="col-sm-2">
                            <label for="value-0">Value</label>
                            <input type="number" class="form-control" name="value" id="value-0">
                        </div>
                        <div class="col-sm-2">
                            <label>Submit</label>
                            <button type="submit" class="btn btn-primary btn-block">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection