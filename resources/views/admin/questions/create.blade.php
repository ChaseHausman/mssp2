@extends('layouts.admin')

@section('title')
    Create Question in Chapter {{ $chapter->number }}
@endsection

@section('content')
    <a href="{{ route('chapters.index', ['book' => $chapter->book_id]) }}"><i class="fa fa-backward"></i> Return to {{ $chapter->book->title }}</a><br>
    <a href="{{ route('questions.index', ['chapter' => $chapter->id]) }}"><i class="fa fa-arrow-circle-left"></i> Return to {{ $chapter->title }}</a>
    <form action="{{ route('questions.store', ['chapter' => $chapter->id]) }}" method="post" class="row mt-4" autocomplete="off">
        <div class="col-md-9">
            <div class=" card card-block">
                <div class="form-group">
                    <label for="text">Question Text</label>
                    <textarea name="text" id="text" class="form-control" rows="6">{{ old('text') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="hint">Hint</label>
                    <textarea name="hint" id="hint" class="form-control">{{ old('hint') }}</textarea>
                </div>
                <div class="card card-block">
                    <h3>Answer Choices</h3>
                    @for($i = 0; $i < 4; $i++)
                        <div class="form-group row option">
                            <div class="col-sm-2">
                                <label for="letter-{{ $i }}">Letter</label>
                                <input type="text" class="form-control" name="letter[{{ $i }}]" id="letter-{{ $i }}" value="{{ old('letter.'.$i) ?? $alpha[$i] }}">
                            </div>
                            <div class="col-sm-8">
                                <label for="answer-{{ $i }}">Option</label>
                                <input type="text" class="form-control" name="answer[{{ $i }}]" id="answer-{{ $i }}" value="{{ old('answer.'.$i) }}" autocomplete="off">
                            </div>
                            <div class="col-sm-2">
                                <label>Correct?</label>
                                <div class="btn-group-toggle" data-toggle="buttons">
                                    <label for="answer-{{ $i }}-correct" class="btn btn-outline-secondary btn-block @if(old('correct.'.$i))active @endif">
                                        <input type="checkbox" class="form-check-input" id="answer-{{ $i }}-correct" name="correct[{{ $i }}]" value="1" @if(old('correct.'.$i)) checked="checked" @endif> Yes
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <label for="explanation-{{ $i }}">Explanation</label>
                                <input type="text" class="form-control" name="explanation[{{ $i }}]" id="explanation-{{ $i }}" value="{{ old('explanation.'.$i) }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="value-{{ $i }}">Value</label>
                                <input type="number" class="form-control" name="value[{{ $i }}]" id="value-{{ $i }}" value="{{ old('value.'.$i) ?? 1 }}">
                            </div>
                        </div>
                        <hr>
                    @endfor
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card card-block">
                <div class="form-group">
                    <label for="points">Points</label>
                    <input type="number" name="points" id="points" class="form-control" value="1">
                </div>
                <div class="form-group">
                    <label for="min-correct">Minimum Correct Points</label>
                    <input type="number" name="min_correct_points" id="min-correct" class="form-control" value="1">
                </div>
                <hr>
                <div class="form-group">
                    {{ csrf_field() }}
                    <input type="hidden" name="chapter" value="{{ $chapter->id }}">
                    <button type="submit" class="btn btn-primary btn-block">Create</button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')

@endsection