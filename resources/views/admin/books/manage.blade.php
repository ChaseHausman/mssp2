@extends('layouts.admin')

@section('title')
    Update {{ $book->title }}
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <form class="card card-block" action="{{ route('books.update', ['book' => $book->id]) }}" method="post">
                <div class="form-group">
                    <label for="title">Book Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{ $book->title }}">
                </div>
                <div class="form-group">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')

@endsection