@extends('layouts.admin')

@section('title')
    Books
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <table class="table table-striped">
                <tbody>
                    @foreach($books as $book)
                        <tr>
                            <th><a href="{{ route('books.show', ['book' => $book->id]) }}">{{ $book->title }}</a></th>
                            <td><a href="{{ route('chapters.index', ['book' => $book->id]) }}">{{ $book->chapters->count() }} {{ str_plural('Chapter', $book->chapters->count()) }}</a></td>
                            <td>
                                <form action="{{ route('books.destroy', ['book' => $book->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <div class="btn-group btn-group-sm">
                                        <a href="{{ route('books.edit', ['book' => $book->id]) }}" class="btn btn-outline-secondary btn-sm">Manage</a>
                                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <div class="card card-block">
                <h2>Create</h2>
                <form action="{{ route('books.store') }}" method="post">
                    <div class="form-group">
                        <label for="title">Book Title</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection