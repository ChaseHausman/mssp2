@extends('layouts.admin')

@section('title')
    All Contact Messages
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td><a href="{{ route('admin.contact', ['id' => $contact->id]) }}">{{ $contact->name }}</a></td>
                            <td><a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a></td>
                            <td>{{ str_limit($contact->message) }}</td>
                            <td>{{ $contact->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $contacts->links('vendor.pagination.bootstrap-4') }}
        </div>
    </div>
@endsection