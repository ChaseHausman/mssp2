@extends('layouts.app')

@section('title')
    /u/{{ $user->handle }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            <h1 class="display-3">{{ $user->handle }} <small class="lead">{{ $user->name }}</small></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <p class="card-text">A user since {{ $user->created_at->format("M 'y") }}.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection