<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatedPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('permissions')->insert([
            ['name' => "manage questions", 'group' => 'MSSP', 'description' => "The ability to add/remove/edit test questions."],
            ['name' => "view results", 'group' => 'MSSP', 'description' => "The ability to see others submissions/results."],
            ['name' => "take tests", 'group' => 'MSSP', 'description' => "The ability to take tests."],
            ['name' => "make tests", 'group' => 'MSSP', 'description' => "The ability to make new tests, and manage their own."],
            ['name' => "manage tests", 'group' => 'MSSP', 'description' => "The ability to manage other peoples tests."],
            ['name' => "manage books", 'group' => 'MSSP', 'description' => "The ability to manage books and their chapters."],
            ['name' => "take quiz", 'group' => 'MSSP', 'description' => "The ability to take a random question quiz."],
        ]);

        \Illuminate\Support\Facades\DB::table('roles')->insert([
            ['name' => "Instructor"],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
