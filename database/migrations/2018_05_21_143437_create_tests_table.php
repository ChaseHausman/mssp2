<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tag');
            $table->boolean('public');
            $table->boolean('specificQuestions');
            $table->integer('questionCount');
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('chapter_test', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('chapter_id');
            $table->integer('test_id');
        });

        Schema::create('test_question', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id');
            $table->integer('test_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
