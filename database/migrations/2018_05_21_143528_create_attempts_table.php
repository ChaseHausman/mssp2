<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('test_id');
            $table->boolean('finished')->default(false);
            $table->float('possible_points')->nullable()->default(null);
            $table->float('total_points')->nullable()->default(null);
            $table->timestamps();
            $table->dateTime('start_at');
            $table->dateTime('stop_at')->nullable()->default(null);
        });

        Schema::create('attempt_question', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id');
            $table->integer('attempt_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attempts');
        Schema::dropIfExists('attempt_question');
    }
}
