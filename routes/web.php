<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');

Route::post('/analytics/view', 'AnalyticsController@view')->name('analytics.view');

Route::get('contact', 'ContactController@general')->name('contact');
Route::get('contact/general', 'ContactController@general');
Route::post('contact', 'ContactController@post')->name('contact.post');

// ## Authentication
\Illuminate\Support\Facades\Auth::routes();
Route::get('login/{service}', 'Auth\SocialLoginController@redirect')->name('auth.social');
Route::get('login/{service}/callback', 'Auth\SocialLoginController@callback')->name('auth.social.callback');

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function() {
    Route::get('/settings', 'Users\PreferencesController@settings')->name('user.settings');
    Route::post('/settings', 'Users\PreferencesController@updateSettings')->name('user.settings.post');

    Route::get('/demographics', 'Users\PreferencesController@demographics')->name('user.demographics');
    Route::post('/demographics', 'Users\PreferencesController@saveDemographics')->name('user.demographics.post');

    Route::get('/notifications', 'NotificationController@all')->name('notifications');
    Route::post('/notifications/mark', 'NotificationController@mark')->name('notifications.mark');
    Route::delete('/notifications/delete', 'NotificationController@delete')->name('notifications.delete');
});

// ## Public Profiles
Route::group(['prefix' => 'u'], function() {
    Route::get('{handle?}', 'Users\ProfileController@profile')->name('profile');
    Route::get('{handle?}/rankings', 'Users\ProfileController@rankings')->name('profile.rankings');
});

// ## Tests
Route::group(['prefix' => 'tests'], function() {
    Route::get('/', 'TestController@index')->name('tests');
    Route::get('create', 'TestController@createNewTest')->name('tests.create');
    Route::post('create', 'TestController@create')->name('tests.create.post');

    Route::get('{test}', 'TestController@landing')->name('tests.landing');
    Route::get('{test}/manage', 'TestController@manage')->name('tests.manage');

    Route::get('{test}/attempt', 'TestController@attempt')->name('tests.attempt');
    Route::get('{test}/take/{attempt}', 'TestController@take')->name('tests.take');
    Route::post('{test}/response', 'TestController@recordResponse')->name('tests.response.post');
    Route::post('{test}/submit/{attempt}', 'TestController@submit')->name('tests.submit.post');
});

// ## Quizzes
Route::group(['prefix' => 'quiz', 'middleware' => ['auth']], function() {
    Route::get('/', 'QuizController@index')->name('quiz');
    Route::get('take/{chapter?}', 'QuizController@take')->name('quiz.take');
    Route::post('attempt', 'QuizController@attempt')->name('quiz.attempt');
});

// ## Admin
Route::group(['middleware' => ['role:Admin', 'auth'], 'prefix' => 'console'], function() {
    Route::get('/', 'AdminController@dashboard')->name('admin');
    Route::get('/search', 'AdminController@search')->name('admin.search');
    Route::get('/contact/{id?}','ContactController@admin')->name('admin.contact');
    Route::get('/homepage', 'HomeController@edit')->name('admin.general.homepage');
    Route::post('/homepage', 'HomeController@save')->name('admin.general.homepage.post');

    Route::group(['middleware' => ['can:Can manage users']], function() {
        Route::get('users', 'Users\UserController@index')->name('admin.user.index');
        Route::get('user/{id}', 'Users\UserController@getUser')->name('admin.user')->where('id', '[0-9]+');
        Route::post('user/update', 'Users\UserController@postUser')->name('admin.user.post');
        Route::delete('user/role/remove', 'Users\PermissionsController@deleteUserRole')->name('admin.user.role.delete');
        Route::post('user/role/grant', 'Users\PermissionsController@postUserRole')->name('admin.user.role.post');
        Route::post('user/permission/grant', 'Users\PermissionsController@postUserPermission')->name('admin.user.permission.post');
        Route::post('user/lock', 'Users\UserController@lockAccount')->name('admin.user.lock.post');
        Route::post('user/unlock', 'Users\UserController@unlockAccount')->name('admin.user.unlock.post');
        Route::post('user/reset', 'Users\UserController@resetPassword')->name('admin.user.reset-password');
    });

    Route::group(['middleware' => ['can:Can manage roles']], function() {
        Route::get('user/roles/{id?}', 'Users\PermissionsController@getRoles')->name('admin.user.roles')->where('id', '[0-9]+');
        Route::post('user/roles', 'Users\PermissionsController@postRole')->name('admin.user.roles.post');
        Route::delete('user/roles', 'Users\PermissionsController@deleteRole')->name('admin.user.roles.delete');
    });

    Route::group(['middleware' => ['can:Can write content'], 'prefix' => 'content'], function() {
        Route::get('pages', 'ContentController@adminPages')->name('admin.content.pages');
        Route::post('page', 'ContentController@savePage')->name('admin.content.page.post');
        Route::get('page/{page?}', 'ContentController@adminPage')->name('admin.content.page');

        Route::get('links/{link?}', 'ContentController@adminLinks')->name('admin.content.links');
        Route::post('links', 'ContentController@saveLink')->name('admin.content.link.post');
        Route::delete('link', 'ContentController@deleteLink')->name('admin.content.link.delete');
    });

    Route::group(['prefix' => 'emailer', 'middleware' => ['role:Admin', 'auth']], function() {
        Route::get('/', 'EmailManagerController@getCampaignManager')->name('admin.emailer');
        Route::get('/manage/{key}', 'EmailManagerController@getSpecificEmailKind')->name('admin.emailer.specific');
        Route::post('/manage/{key}', 'EmailManagerController@postSendSpecificEmail')->name('admin.emailer.send');
        Route::post('/manage/{key}/preview', 'EmailManagerController@preview')->name('admin.emailer.preview');
    });

    Route::resources([
        'books' => 'BookController',
        'answers' => 'AnswerController'
    ]);

    Route::get('books/{book}/chapters', 'ChapterController@index')->name('chapters.index');
    Route::get('books/{book}/chapters/{chapter}', 'ChapterController@show')->name('chapters.show');
    Route::post('/books/{book}/chapters', 'ChapterController@store')->name('chapters.store');
    Route::get('books/{book}/chapters/{chapter}/edit', 'ChapterController@edit')->name('chapters.edit');
    Route::put('books/{book}/chapters/{chapter}', 'ChapterController@update')->name('chapters.update');
    Route::delete('books/{book}/chapters/{chapter}', 'ChapterController@destroy')->name('chapters.destroy');

    Route::get('chapters/{chapter}/questions', 'QuestionController@index')->name('questions.index');
    Route::get('chapters/{chapter}/questions/create', 'QuestionController@create')->name('questions.create');
    Route::post('chapters/{chapter}/questions', 'QuestionController@store')->name('questions.store');
    Route::get('chapters/{chapter}/questions/{question}', 'QuestionController@show')->name('questions.show');
    Route::get('chapters/{chapter}/questions/{question}/edit', 'QuestionController@edit')->name('questions.edit');
    Route::put('chapters/{chapter}/questions/{question}', 'QuestionController@update')->name('questions.update');
    Route::delete('chapters/{chapter}/questions/{question}', 'QuestionController@destroy')->name('questions.destroy');
});

// Email
Route::group(['prefix' => 'emails'], function() {
    Route::group(['prefix' => 'mailgun'], function() {
        Route::post('/unsubscribe', 'EmailedController@mailgunUnsubscribe');
    });
});

// ## CMS?
Route::get('{page}', 'ContentController@page')->name('content');